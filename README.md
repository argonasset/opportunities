# Opportunities at Argon Asset Management #

[Argon Asset Management](http://argonassetmanagement.co.za/) is looking for a
talented Data Scientist/Analyst/Engineer to help us organise our data and
build awesome predictive models! The position is part of the **Analytics and Customised Solutions** 
team and your role will be to help us gain data
driven insights for our investment process. Our team currently manages in
excess of R3 billion of retirement funds and medical scheme assets and your
work will directly impact on the savings of thousands of people.

We use the Python Data Science stack (numpy, pandas, matplotlib, sklearn) on
Ubuntu as well as Docker for deployment and environment management.
 
To get a feel for some of the things we do with data, have a look at some of
our public talks:

  * Talk at CTPUG on **Neural Networks in Python** 
      * [code](https://github.com/snth/ctdeep/blob/master/CTPUG%20Talk%202016-05-14.ipynb)
  * Talk at PyconZA 2015 on **The Split-Apply-Combine Pattern
    for Data Science in Python**
      * [video](https://www.youtube.com/watch?v=TjuRnguO62E).
      * [code](https://github.com/snth/split-apply-combine)

If you are interested and think you have what it takes then you should attempt
our [Data Science Questions](
https://bitbucket.org/argonasset/opportunities/src/master/Data_Science-Questions.rst?fileviewer=file-view-default)

  * [Job Spec](http://argonassetmanagement.co.za/sustainable-livelihoods/argon-opportunities/)
 
To apply, answer the [Data Science Questions](
https://bitbucket.org/argonasset/opportunities/src/master/Data_Science-Questions.rst?fileviewer=file-view-default)
and email the repository link to <tobias@argonasset.co.za>. Be sure to add the 'argon_recruitment_rsa_key.pub' to the Deployment Keys or we won't be able to access your repository.
 
Thanks
