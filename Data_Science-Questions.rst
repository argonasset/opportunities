
Data Science - Questions
========================

Part 1: Analytics
-----------------

An investor will split his wealth amongst a number of different
investments. Say he has R100m, he might invest R50m in the stock market
(Equities) and invest R50m in government debt (Bonds). From the
perspective of evaluating the performance of his investments, we usually
only care about what proportion of his wealth is allocated to each of
the investments. In our example the investor had half his wealth in each
of the two asset classes. We can represent this with an array
:math:`(0.5, 0.5)` and call this a **portfolio**.

We often want to compare how different portfolios would have performed
in the past. One strategy for this is to generate all possible
portfolios that sastisfy some condition. For example there are 3 ways to
split one's wealth across Equities and Bonds in chunks of 50%, i.e.
:math:`(0.0, 1.0)`, :math:`(0.5, 0.5)` and :math:`(1.0, 0.0)`, or in
Python:

.. code:: python

    import numpy as np
    A = np.array([[ 0.5*0, 0.5*2],
                  [ 0.5*1, 0.5*1],
                  [ 0.5*2, 0.5*0]])
    print A

.. parsed-literal::

    [[ 0.   1. ]
     [ 0.5  0.5]
     [ 1.   0. ]]


Similarly, there are 5 ways to allocate to two investments in chunks of
25%:

.. code:: python

    B = np.array([[ 0.25*0,  0.25*4],
                  [ 0.25*1,  0.25*3],
                  [ 0.25*2,  0.25*2],
                  [ 0.25*3,  0.25*1],
                  [ 0.25*4,  0.25*0]])
    print B

.. parsed-literal::

    [[ 0.    1.  ]
     [ 0.25  0.75]
     [ 0.5   0.5 ]
     [ 0.75  0.25]
     [ 1.    0.  ]]


How many ways are there to allocate to three asset classes (Equities,
Bonds and Cash) in chunks of

-  50%
-  25%
-  10%
-  5%
-  2%
-  1%

You can work this out analytically or use the next question to help you
answer this.

Part 2: Python (Numpy)
----------------------

Write a Python function ``get_portfolios`` that takes as inputs the
number of investments ``num_investments`` and chunk size ``chunk_size``
and which returns a 2-dimensional Numpy ndarray that contains all
possible portfolios for those parameters.

To be more specific, each portfolio must satisfy the following
conditions:

-  Each portfolio is an array of size ``num_investments``
-  The weight in each investment must be a multiple of the
   ``chunk_size``.
-  The weight in each investment must be between 0 and 1 (both
   included).
-  The weights of all investments must sum to 1.0.

The 2-d array should be lexicographically sorted in ascending order as
in the examples in Part 1.

.. code:: python

    def get_portfolios(num_investments, chunk_size):
        if num_investments==2 and chunk_size==0.5:
            return np.array([[ 0.5*0, 0.5*2],
                             [ 0.5*1, 0.5*1],
                             [ 0.5*2, 0.5*0]])
        elif num_investments==2 and chunk_size==0.25:
            return np.array([[ 0.25*0,  0.25*4],
                             [ 0.25*1,  0.25*3],
                             [ 0.25*2,  0.25*2],
                             [ 0.25*3,  0.25*1],
                             [ 0.25*4,  0.25*0]])
        else:
            raise NotImplementedError()
You can use the following hashes to check your results:

.. code:: python

    from hashlib import md5
    
    def calc_hash(portfolio_array):
        return md5(portfolio_array.astype('float64').round(8)).hexdigest()
    
    print "num_investments={}, chunk_size={:.2f}: {}".format(2, 0.5, calc_hash(get_portfolios(2, 0.5)))
    print "num_investments={}, chunk_size={:.2f}: {}".format(2, 0.25, calc_hash(get_portfolios(2, 0.25)))

.. parsed-literal::

    num_investments=2, chunk_size=0.50: fa8ea83263a971f601d654aa36866454
    num_investments=2, chunk_size=0.25: c6f582a53ab9a7d970b75a2628675ea7


And for 3 investments:

::

    num_investments=3, chunk_size=0.50: 23870d2caea1e1200964ae384ae19331
    num_investments=3, chunk_size=0.25: 9e0338bb81c281f86ee76339b872e686
    num_investments=3, chunk_size=0.10: e900d25947d4c1a42e774a1641a7e1b3
    num_investments=3, chunk_size=0.05: d233a2a45701b4c74dbc482ac2ae3d94
    num_investments=3, chunk_size=0.02: 367d8ff81c586c1e5ea42d3d7184f524
    num_investments=3, chunk_size=0.01: 3fa2838d76a6c6ecf24ff82a71b1d7f9

Part 3: Git
-----------

1. Clone this repository from
   https://bitbucket.org/argonasset/opportunities.git.
2. Check out the ``answers`` branch.
3. Put your function in a file called ``portfolios.py``.
4. Commit your changes to the repository.
5. Open the ``Data_Science-Answers.ipynb`` Jupyter Notebook and fill in
   your answers to Part 1 in the appropriate place. If you are not
   familiar with Jupyter Notebooks you can also put your answers in the
   text file named ``Data_Science-Answers.txt`` under ``Part 1``.
6. Save your answers and commit your changes to the ``answers`` branch.
7. Rebase the ``answers`` branch onto the ``develop`` branch and
   appropriately deal with any merge conflicts.

Part 4: Data
------------

We want to investigate how the number of portfolios increases as we
increase the number of investments and decrease the chunk size. Use the
code below as a starting point to investigate these relationships. You
will soon find that you will run into performance problems. See if you
can improve your program and algorithm to be able generate more results.
Commit your changes to your repository as you go along to show how you
go about improving and refactoring your code.

In the end you should write your results to a csv file named
``results.csv`` that contains as many (unique) results as possible. Add
this file to your repository and commit it to the ``answers`` branch.

Note: You may use any trick you like to improve your performance, i.e.
libraries, compilers, parallelisation, whatever you can think of. Just
be sure to include your code in your submission and include the hash of
the numpy array in the file so that we can verify your results.

.. code:: python

    import time
    
    template = "{:3}, {:<4}, {:8}, {}, {}"
    print template.format('num_investments', 'chunk_size', 'num_portfolios', 'hash', 'time_taken')
    for chunk_size in [0.5, 0.25, 0.1, 0.05, 0.02, 0.01]:
        for num_investments in xrange(2, 21):
            t1 = time.time()
            try:
                portfolio_array = get_portfolios(num_investments, chunk_size)
            except NotImplementedError:
                break
            t2 = time.time()
            print template.format(num_investments, chunk_size, len(portfolio_array)
                                  , calc_hash(portfolio_array), np.round(t2-t1, 4))

.. parsed-literal::

    num_investments, chunk_size, num_portfolios, hash, time_taken
      2, 0.5 ,        3, fa8ea83263a971f601d654aa36866454, 0.0
      2, 0.25,        5, c6f582a53ab9a7d970b75a2628675ea7, 0.0


Part 5: Visualisation
---------------------

Create a visualisation that best summarises your results. You can create
a simple chart in Python with ``matplotlib``, ``seaborn`` or some other
Python package. Alternatively you can use ``d3.js`` or some tool. Save
your visualisation in a file that will be easy for us to view and add it
to the ``answers`` branch in your repository.

Part 6: Docker
--------------

Write a Dockerfile to dockerize your program. This should set up the
necessary environment so that we can run your program on our systems.
Add the Dockerfile to your repository and commit it to the ``answers``
branch.

Part 7: Discussion
------------------

What do your results tell you about generating all possible portfolios
that allocate to the 160 shares in the JSE All Share index in chunks of
1%? If we can trade the portfolio once a day and there are roughly 250
trading days in a year, how many possible trading strategies are there
over the course of one year? How would you recommend we compare the
performance of an investment strategy to the performance of all possible
investment strategies over one year?

Write about one paragraph outlining your thoughts on this. There are no
right answers here so just write what you think.

Add your thoughts either to the ``Data_Science-Answers.ipynb`` notebook
or to text file named ``Data_Science-Answers.txt`` under ``Part 7``.
Commit your changes to the ``answers`` branch.

Part 8: Submitting your application
-----------------------------------

Create a repository on `BitBucket <bitbucket.org>`__ (you can either
fork the ``argonasset`` one or create a new one) and push your commits
to it. Please make it a private repository so that other applicants
cannot access your code. Add our public ssh key (in the file
``argon_recruitment_rsa_key.pub``) to the list of deployment keys and
send us the URL so that we can clone your repository.

Not everyone will have expertise in all parts of this. For example
Docker is still quite new and you might not have worked with it yet.
That is ok! Just leave out that section and submit your work for
whatever parts that you can answer. The idea is to get a feeling for
what your strengths are and you are not expected to know everything from
the start. If you are not familiar with git then please email us your
files.

Good luck!
